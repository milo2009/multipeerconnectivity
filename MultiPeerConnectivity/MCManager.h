//
//  MCManager.h
//  MultiPlayerConnectivity
//
//  Created by Fred on 30/03/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "MultiPlayerNetwork.h"

@interface MCManager : NSObject <MCSessionDelegate, MultiPlayerManager>

@property (nonatomic, strong) MCPeerID *peerID;
@property (nonatomic, strong) MCSession *session;
@property (nonatomic, strong) MCBrowserViewController *browser;
@property (nonatomic, strong) MCAdvertiserAssistant *advertiser;

+ (instancetype)sharedMCManager;
- (void)findMatchWithMinPlayers:(int)minPlayers maxPlayers:(int)maxPlayers viewController:(id)vc;
@end
