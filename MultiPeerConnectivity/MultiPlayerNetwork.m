//
//  MultiPlayerNetwork.m
//  MultiPlayerConnectivity
//
//  Created by Fred on 30/03/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import "MultiPlayerNetwork.h"

@interface MultiPlayerNetwork ()
- (Player*)findPlayerByRandomNumber:(NSNumber*)rn;
- (void)updatePlayer:(Player*)p;
- (void)definePlayerOrder:(NSString*)name withRandomNumber:(NSNumber*)randomNumber;
@end

@implementation MultiPlayerNetwork
+ (instancetype)sharedMultiPlayerNetwork
{
    static MultiPlayerNetwork *sharedMultiPlayerNetwork;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMultiPlayerNetwork = [[MultiPlayerNetwork alloc] init];
    });
    return sharedMultiPlayerNetwork;
}

#pragma mark - Override
- (id)init{
    if (self = [super init]) {
        self.localPlayer = [[Player alloc] initPlayer:[UIDevice currentDevice].name
                               withRandomNumber:[NSNumber numberWithUnsignedInt:arc4random()]];
        self.othersPlayer = [NSMutableArray new];
    }
    
    return self;
}

#pragma mark - Privates methods
- (void)changeState:(PlayerState)state{
    if(self.localPlayer.state >= state)
        return;

    self.localPlayer.state = state;
    [self.manager sendMessage:@{playerKey: self.localPlayer}];
}

- (void)receiveInfos:(NSDictionary*)infos from:(NSString*)name{
    Player *adv = infos[playerKey];
   
    //Si on recoit un message de deconnection, on fini la partie
    if (adv.state == kStateDisconnected){
        dispatch_async(dispatch_get_main_queue(), ^{ [_delegate gameEnd]; });
        return;
    }
    
    //On traite le message recu
    if(adv.state == kStateWaitingForRandomNumber){
        [self definePlayerOrder:adv.name
               withRandomNumber:adv.randomNumber];
    }
    else{
        [self updatePlayer:infos[playerKey]];
        
        //Si le joueur local n'a pas encore chargé la view de jeu, on n'essaye pas de la mettre à jour
        if(self.localPlayer.state == kStateIsReady)
            return;
        
        //On envoie à la vue les données reçus
        switch (adv.state) {
            case kStateWaitingForStart:{
                dispatch_async(dispatch_get_main_queue(), ^{ [_delegate gameInit]; });
                
                break;
            }
                
            case kStateInGame:
            {
                if(self.localPlayer.state == kStateInGame){
                    //MAJ du score de l'adversaire
                    dispatch_async(dispatch_get_main_queue(), ^{ [_delegate updateScore:adv.score
                                                                               andLives:adv.lives]; });
                }
                else if((self.isFirstPlayer == NO) && (self.localPlayer.state == kStateWaitingForStart)){
                    [self changeState:kStateInGame];
                    
                    //Demarre la partie
                    dispatch_async(dispatch_get_main_queue(), ^{ [_delegate gameBegin]; });
                }
                
                break;
            }
                
            case kStateFinish:
                //Arrete la partie si elle n'est pas deja arrété
                if(self.localPlayer.state < kStateFinish)
                    dispatch_async(dispatch_get_main_queue(), ^{ [_delegate gameEnd]; });
                
                break;
            
            default:
                break;
        }
    }
}

- (void)definePlayerOrder:(NSString*)name withRandomNumber:(NSNumber*)randomNumber{
    //Attend le random number de l'adversaire
    if(self.localPlayer.state == kStateWaitingForRandomNumber){
        
        //S'il sont egaux ou nil, on change et on le renvoie
        if(([self.localPlayer.randomNumber isEqualToNumber:randomNumber]) || (randomNumber == nil)){
            self.localPlayer.randomNumber = [NSNumber numberWithUnsignedInt:arc4random()];
            [self changeState:kStateWaitingForRandomNumber];
        }
        else{
            //On a déterminé l'ordre, on passe en pret
            self.isFirstPlayer = ([self.localPlayer.randomNumber intValue] > [randomNumber intValue]);
            self.localPlayer.state = kStateIsReady;
            
            //On a déterminé l'ordre, on sait que le joureur est pret
            Player *adv = [[Player alloc] initPlayer:name
                                    withRandomNumber:randomNumber];
            adv.state = kStateIsReady;
            
            //On l'ajoute à la liste des joueurs
            [self.othersPlayer addObject:adv];
        }
    }
}

- (Player*)findPlayerByRandomNumber:(NSNumber*)rn{
    NSPredicate *findRNP = [NSPredicate predicateWithFormat:@"randomNumber == %@", rn];
    return [[self.othersPlayer filteredArrayUsingPredicate:findRNP] firstObject];
}

- (void)updatePlayer:(Player*)p{
    NSUInteger index = [self.othersPlayer indexOfObject:[self findPlayerByRandomNumber:p.randomNumber]];
    [self.othersPlayer replaceObjectAtIndex:index
                        withObject:p];
}

- (BOOL)allPlayerInState:(PlayerState)state{
    NSPredicate *pWFS = [NSPredicate predicateWithBlock:^BOOL(Player* evaluatedObject, NSDictionary *bindings) {
        return (evaluatedObject.state == state);
    }];
    
    BOOL othersPlayerWFS = ([self.othersPlayer filteredArrayUsingPredicate:pWFS].count == self.othersPlayer.count);
    return ((self.localPlayer.state == state) && othersPlayerWFS);
}

- (void)sendScore:(int)score{
    self.localPlayer.score = [NSNumber numberWithInt:score];
    
    [self.manager sendMessage:@{playerKey: self.localPlayer}];
}

- (void)sendLives:(int)lives{
    self.localPlayer.lives = [NSNumber numberWithInt:lives];
    
    [self.manager sendMessage:@{playerKey: self.localPlayer}];
}

- (MatchResult)getResult{
    //Si on a perdu la connection, on retourne le status d'erreur
//    if(self.localPlayer.state == kStateDisconnected)
//        return kMRError;
    
    //Sinon, on recupere le plus grand score des adversaires pour ce comparer à lui
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"score"
                                                                 ascending:NO];
    NSArray *results = [self.othersPlayer sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    if ([self.localPlayer.score intValue] < [[results.firstObject valueForKey:@"score"] intValue]) {
        return kMRLost;
    }
    else if ([self.localPlayer.score intValue] > [[results.firstObject valueForKey:@"score"] intValue]) {
        return kMRWon;
    }
    else
        return kMRDraw;
}

- (void)disconnect:(BOOL)onError{
    //Si on est deja déconnecté, on ne fait rien
    if(self.localPlayer.state == kStateDisconnected)
        return;
    
    self.localPlayer.state = kStateDisconnected;
        
    //S'il ne s'agit pas d'une erreur, on previent le manager que la partie est finie
    if(onError == NO)
        [self.manager disconnect:onError];
    else
        //S'il s'agit d'une erreur, on previent la vue d'arreter la partie
        dispatch_async(dispatch_get_main_queue(), ^{ [_delegate gameEnd]; });
}
@end
