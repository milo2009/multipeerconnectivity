//
//  GameController.h
//  MultiPeerConnectivity
//
//  Created by Fred on 20/10/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultiPlayerNetwork.h"

@interface GameController : UIViewController<MultiplayerNetworkingProtocol>
@property (weak, nonatomic) IBOutlet UILabel *lbWait;

@property (weak, nonatomic) IBOutlet UILabel *lbLevel;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbMyName;
@property (weak, nonatomic) IBOutlet UILabel *lbAdvName;
@property (weak, nonatomic) IBOutlet UILabel *lbAdvScore;
@property (weak, nonatomic) IBOutlet UILabel *lbMyScore;

@property (weak, nonatomic) IBOutlet UILabel *lbCalcul;
@property (weak, nonatomic) IBOutlet UIButton *btnAnswer1;
@property (weak, nonatomic) IBOutlet UIButton *btnAnswer2;
@property (weak, nonatomic) IBOutlet UIButton *btnAnswer3;

@property (weak, nonatomic) IBOutlet UIButton *btStart;
@end
