//
//  AppDelegate.h
//  MultiPeerConnectivity
//
//  Created by Fred on 20/10/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

