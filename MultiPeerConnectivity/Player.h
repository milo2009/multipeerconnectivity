//
//  Player.h
//  MultiPlayerConnectivity
//
//  Created by Fred on 01/04/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, PlayerState) {
    kStateWaitingForMatch = 0,
    kStateWaitingForRandomNumber,
    kStateIsReady,
    kStateWaitingForStart,
    kStateInGame,
    kStateFinish,
    kStateDisconnected
};

@interface Player : NSObject<NSCoding>
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *randomNumber;
@property (nonatomic) PlayerState state;
@property (nonatomic, strong) NSNumber *score;
@property (nonatomic, strong) NSNumber *lives;

- (id)initPlayer:(NSString*)name withRandomNumber:(NSNumber*)rn;
@end
