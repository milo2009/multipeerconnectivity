//
//  MultiPlayerNetwork.h
//  MultiPlayerConnectivity
//
//  Created by Fred on 30/03/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"

//Data send keys
#define matchMakerStateKey @"Match-Maker-State-Key"
#define playerKey @"Player-Key"

typedef NS_ENUM(NSUInteger, MatchMakerState) {
    kMMStateCancel=0,
    kMMStateError,
    kMMStateFound
};

typedef NS_ENUM(NSUInteger, MatchResult) {
    kMRError=0,
    kMRLost,
    kMRWon,
    kMRDraw
};

@protocol MultiplayerNetworkingProtocol <NSObject>
- (void)gameInit;
- (void)gameBegin;
- (void)updateScore:(NSNumber*)score andLives:(NSNumber*)lives;
- (void)gameEnd;
@end

@protocol MultiPlayerManager
- (void)sendMessage:(NSDictionary *)dict;
- (void)disconnect:(BOOL)onError;
@end

@interface MultiPlayerNetwork : NSObject
@property (weak, nonatomic) id<MultiPlayerManager> manager;
@property (weak, nonatomic) id<MultiplayerNetworkingProtocol> delegate;
@property (nonatomic) BOOL isFirstPlayer;
@property (nonatomic, strong) Player *localPlayer;
@property (nonatomic, strong) NSMutableArray *othersPlayer;

+ (instancetype)sharedMultiPlayerNetwork;

- (void)changeState:(PlayerState)state;
- (void)receiveInfos:(NSDictionary*)infos from:(NSString*)name;
- (BOOL)allPlayerInState:(PlayerState)state;
- (void)sendScore:(int)score;
- (void)sendLives:(int)lives;
- (MatchResult)getResult;

- (void)disconnect:(BOOL)onError;
@end
