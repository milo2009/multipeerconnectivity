//
//  GameController.m
//  MultiPeerConnectivity
//
//  Created by Fred on 20/10/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import "GameController.h"
#import "Player.h"

#define additionsPerLevel 5

@interface GameController ()
//************** VAR ***************//
@property (nonatomic, strong) MultiPlayerNetwork *delegate;

// This object that will be used to count the 60 seconds of each level.
@property (nonatomic, strong) NSTimer *gameTimer;

// These two member variables that will store the operand values of the addition.
@property (nonatomic) int operand1;
@property (nonatomic) int operand2;

// The timer value.
@property (nonatomic) int timerValue;

// The current level.
@property (nonatomic) int level;

// The current round of a level.
@property (nonatomic) int currentAdditionCounter;

// The player's score.
@property (nonatomic) int score;

// The number of remaining "lives" in the game.
@property (nonatomic) int lives;

//************** UI ***************//
@property (weak, nonatomic) IBOutlet UIView *cvAnswer;

- (IBAction)startGame:(id)sender;
- (IBAction)handleAnswer:(id)sender;
@end

@implementation GameController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.navigationController.navigationBar setHidden:YES];
    
    //We get the instance of MultiPlayerNetwork delegate
    _delegate = [MultiPlayerNetwork sharedMultiPlayerNetwork];
    
    [_delegate changeState:kStateWaitingForStart];
    [self gameInit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (IBAction)startGame:(id)sender {
    if(_delegate.localPlayer.state == kStateWaitingForStart){
        //We inform the other player that player 1 has start the game
        [_delegate changeState:kStateInGame];
        
        [self gameBegin];
    }
}

- (IBAction)handleAnswer:(id)sender {
    // Get the sender's title and check if it matches to the correct result.
    int answer = [[(UIButton *)sender titleForState:UIControlStateNormal] intValue];
    
    // Declare and init a flag that will indicate whether the game should continue after a
    // player selects wrong answer.
    BOOL shouldContinue = YES;
    
    if (answer == _operand1 + _operand2) {
        // In case of a correct answer, then add 10 more points to the score and update
        // the lblScore label.
        _score += 10;
        [_lbMyScore setText:[NSString stringWithFormat:@"%i", _score]];
        
        //Send the new score to the opponent
        [_delegate sendScore:_score];
    }
    else{
        // If the player select a wrong answer, then decrease the available amount of lives by one.
        UIImageView *imgLife = (UIImageView*)[self.view viewWithTag:_lives];
        imgLife.hidden = YES;
        _lives--;
        
        //On envoie le nb de vie restante à l'adversaire
        [_delegate sendLives:_lives];
        
        // If no more lives have been left, the game must stop.
        if (_lives == 0) {
            // Indicate that the game should not continue.
            shouldContinue = NO;
            
            [self gameEnd];
        }
        
    }
    
    // The next part will be executed only if the game is still on.
    if (shouldContinue) {
        //Create a new random addition.
        [self createAddition];
        
        // Increase the round counter value by one.
        _currentAdditionCounter++;
        
        // If the counter becomes equal to the allowed additions per level, then set its initial value,
        // update the level and restart the timer.
        if (_currentAdditionCounter == additionsPerLevel) {
            _currentAdditionCounter = 0;
            
            //restart the timer
            [self startTimer];
            
            //Update labels
            [self updateLevelLabel];
        }
    }
}

#pragma mark - Privates methods
- (void)initGame{
    self.lbMyName.text = _delegate.localPlayer.name;
    self.lbMyScore.text = @"0";
    self.lbAdvName.text = (_delegate.othersPlayer.count > 0) ? ((Player*)_delegate.othersPlayer[0]).name : @"Adversaire";
    self.lbAdvScore.text = @"0";
    
    [self initValues];
    [self initLives];
    
    //If every one is not ready, we wait
    if(![_delegate allPlayerInState:kStateWaitingForStart])
        return;
    
    //If i'm not the player 1, i wait
    if(_delegate.isFirstPlayer == NO){
        self.lbWait.text = @"Joueur 1 va demarrer la partie...";
        self.lbWait.hidden = NO;
        self.btStart.hidden = YES;
    }
    else{
        self.lbWait.text = @"En attente des autres joueurs...";
        self.lbWait.hidden = YES;
        self.btStart.hidden = NO;
    }
}

-(void)initValues{
    // Set the initial values to all member variables.
    _timerValue = 60;
    _level = 1;
    _currentAdditionCounter = 0;
    _score = 0;
    _lives = 3;
}

-(void)initLives{
    // Set the initial value to the lives property and make all images visible.
    _lives = 3;
    
    for (int i = 1; i<=3; i++) {
        UIImageView *imgLife = (UIImageView*)[self.view viewWithTag:i];
        imgLife.hidden = NO;
    }
}

-(void)createAddition{
    // Generate two random integer numbers.
    _operand1 = arc4random() % 101;
    _operand2 = arc4random() % 21;
    
    // Create the addition string and set it to the lblAddition label.
    [_lbCalcul setText:[NSString stringWithFormat:@"%d + %d", _operand1, _operand2]];
    
    // Calculate the correct result.
    int correctResult = _operand1 + _operand2;
    
    // Produce two more random results.
    int randomResult1 = arc4random() % 121;
    int randomResult2 = arc4random() % 121;
    
    // Pick randomly the button on which the correct answer will appear.
    int randomButton = arc4random() % 3;
    
    switch (randomButton) {
        case 0:
            [_btnAnswer1 setTitle:[NSString stringWithFormat:@"%d", correctResult] forState:UIControlStateNormal];
            [_btnAnswer2 setTitle:[NSString stringWithFormat:@"%d", randomResult1] forState:UIControlStateNormal];
            [_btnAnswer3 setTitle:[NSString stringWithFormat:@"%d", randomResult2] forState:UIControlStateNormal];
            break;
        case 1:
            [_btnAnswer1 setTitle:[NSString stringWithFormat:@"%d", randomResult1] forState:UIControlStateNormal];
            [_btnAnswer2 setTitle:[NSString stringWithFormat:@"%d", correctResult] forState:UIControlStateNormal];
            [_btnAnswer3 setTitle:[NSString stringWithFormat:@"%d", randomResult2] forState:UIControlStateNormal];
            break;
        case 2:
            [_btnAnswer1 setTitle:[NSString stringWithFormat:@"%d", randomResult1] forState:UIControlStateNormal];
            [_btnAnswer2 setTitle:[NSString stringWithFormat:@"%d", randomResult2] forState:UIControlStateNormal];
            [_btnAnswer3 setTitle:[NSString stringWithFormat:@"%d", correctResult] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
}

-(void)startTimer{
    if([_gameTimer isValid])
        [_gameTimer invalidate];
    
    // Set the initial value to the timerValue property and start the timer.
    _timerValue = 60;
    
    _gameTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateTimerLabel:)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)updateTimerLabel:(NSTimer *)timer{
    // Increase the timer value and set it to the lblTime label.
    _timerValue--;
    
    [_lbTime setText:[NSString stringWithFormat:@"%d", _timerValue]];
    
    // If the timerValue value becomes greater than 60 then end the game.
    if (_timerValue == 0)
        [self gameEnd];
}

-(void)updateLevelLabel{
    // Increase the level counter by 1 and show it to the level label.
    _level++;
    [_lbLevel setText:[NSString stringWithFormat:@"Level %d", _level]];
    [_lbTime setText:[NSString stringWithFormat:@"%d", _timerValue]];
}

#pragma mark - MultiplayerNetworkProtocol delegates
-(void)gameInit{
    [self performSelectorOnMainThread:@selector(initGame)
                           withObject:nil
                        waitUntilDone:NO];
}

-(void)gameBegin{
    //We skip this if the game has already started
    if([_gameTimer isValid])
        return;
    
    [self startTimer];
    
    self.lbWait.hidden = YES;
    self.btStart.hidden = YES;
    
    // Create a random addition.
    [self createAddition];
    
    // Make all buttons visible.
    self.cvAnswer.hidden = NO;
}

-(void)updateScore:(NSNumber*)score andLives:(NSNumber*)lives{
    _lbAdvScore.text = [NSString stringWithFormat:@"%@", score];
    
    NSInteger tag = [lives integerValue] + 10;
    UIImageView *imgLife = (UIImageView*)[self.view viewWithTag:tag];
    imgLife.hidden = YES;
}

-(void)gameEnd{
    NSString *result;
    
    //The game is already finish ?
    if([_gameTimer isValid]){
        [_gameTimer invalidate];
        
        self.cvAnswer.hidden = YES;
        self.lbWait.text = @"En attente des autres joueurs...";
        self.lbWait.hidden = NO;
        
        //We inform the others players
        [_delegate changeState:kStateFinish];
        
        //Display the result
        switch ([_delegate getResult]) {
            case kMRLost:
                result = @"Vous avez perdu";
                break;
                
            case kMRDraw:
                result = @"Egalité";
                break;
                
            case kMRWon:
                result = @"Vous avez gagné";
                break;
                
            default:
                result = @"Une erreur est survenue";
                break;
        }
        
        //Cut the connection
        [_delegate disconnect:NO];

        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Partie terminée"
                                              message:result
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       dispatch_async(dispatch_get_main_queue(),
                                                      ^{ [self.navigationController popViewControllerAnimated:YES]; });
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];        
    }
}

@end
