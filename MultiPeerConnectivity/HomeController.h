//
//  HomeController.h
//  MultiPeerConnectivity
//
//  Created by Fred on 20/10/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "MCManager.h"

@interface HomeController : UIViewController<MCBrowserViewControllerDelegate>


@end

