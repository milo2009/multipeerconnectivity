//
//  Player.m
//  MultiPlayerConnectivity
//
//  Created by Fred on 01/04/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import "Player.h"
#define ASCPlayerName @"name"
#define ASCPlayerRandomNumber @"rn"
#define ASCPlayerState @"state"
#define ASCPlayerScore @"score"
#define ASCPlayerLives @"lives"

@implementation Player
- (id)initPlayer:(NSString*)name withRandomNumber:(NSNumber*)rn{
    self = [super init];
    
    if (self) {
        self.name = name;
        self.randomNumber = rn;
        self.score = [NSNumber numberWithInt:0];
        self.state = kStateWaitingForMatch;
        self.lives = [NSNumber numberWithInt:3];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        self.state = [[aDecoder decodeObjectForKey:ASCPlayerState] unsignedIntValue];
        self.name = [aDecoder decodeObjectForKey:ASCPlayerName];
        self.randomNumber = [aDecoder decodeObjectForKey:ASCPlayerRandomNumber];
        self.score = [aDecoder decodeObjectForKey:ASCPlayerScore];
        self.lives = [aDecoder decodeObjectForKey:ASCPlayerLives];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:@(self.state) forKey:ASCPlayerState];
    [aCoder encodeObject:self.name forKey:ASCPlayerName];
    [aCoder encodeObject:self.randomNumber forKey:ASCPlayerRandomNumber];
    [aCoder encodeObject:self.score forKey:ASCPlayerScore];
    [aCoder encodeObject:self.lives forKey:ASCPlayerLives];
}
@end
