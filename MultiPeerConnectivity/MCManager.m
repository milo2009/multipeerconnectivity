//
//  MCManager.m
//  MultiPlayerConnectivity
//
//  Created by Fred on 30/03/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import "MCManager.h"
#define ServiceType @"MPC-message"

@interface MCManager ()
//@property (strong, nonatomic) u
@end

@implementation MCManager

+ (instancetype)sharedMCManager
{
    static MCManager *sharedMCManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMCManager = [[MCManager alloc] init];
    });
    return sharedMCManager;
}

-(id)init{
    self = [super init];
    
    if (self) {
        [self initManager];
    }
    
    return self;
}

#pragma mark - private method implementation
-(void) initManager{
    _peerID = nil;
    _session = nil;
    _browser = nil;
    _advertiser = nil;
    
    [self setupPeerAndSessionWithDisplayName:[UIDevice currentDevice].name];
    [self advertiseSelf:YES];
}

#pragma mark - Public method implementation

-(void)setupPeerAndSessionWithDisplayName:(NSString *)displayName{
    _peerID = [[MCPeerID alloc] initWithDisplayName:displayName];
    
    _session = [[MCSession alloc] initWithPeer:_peerID];
    _session.delegate = self;
}

-(void)advertiseSelf:(BOOL)shouldAdvertise{
    if (shouldAdvertise) {
        _advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:ServiceType
                                                           discoveryInfo:nil
                                                                 session:_session];
        [_advertiser start];
    }
    else{
        [_advertiser stop];
        _advertiser = nil;
    }
}

- (void)findMatchWithMinPlayers:(int)minPlayers maxPlayers:(int)maxPlayers viewController:(id)vc{
    _browser = [[MCBrowserViewController alloc] initWithServiceType:ServiceType
                                                            session:_session];
    _browser.minimumNumberOfPeers = minPlayers;
    _browser.maximumNumberOfPeers = maxPlayers;
    _browser.delegate = vc;
    
    [vc presentViewController:_browser
                     animated:YES
                   completion:nil];
}


#pragma mark - MultiPlayerManager delegate
-(void)sendMessage:(NSDictionary *)dict{
    NSArray *allPeers = _session.connectedPeers;
    NSError *error;
    
    [_session sendData:[NSKeyedArchiver archivedDataWithRootObject:dict]
               toPeers:allPeers
              withMode:MCSessionSendDataReliable
                 error:&error];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
        [self disconnect:YES];
    }
}

-(void)disconnect:(BOOL)onError{
    //On coupe la connexion
    [_session disconnect];
    //On re-init le manager
    [self initManager];
    
    //ON ne previent la vue que s'il s'agit d'une erreur et non d'une fin de partie
    if(onError == YES)
        [[MultiPlayerNetwork sharedMultiPlayerNetwork] disconnect:onError];
}

#pragma mark - MCSession Delegate method implementation

-(void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state{
    if (state != MCSessionStateConnecting) {
        if (state == MCSessionStateConnected) {
            [[MultiPlayerNetwork sharedMultiPlayerNetwork] changeState:kStateWaitingForRandomNumber];
        }
        else if (state == MCSessionStateNotConnected){
            if ([MultiPlayerNetwork sharedMultiPlayerNetwork].localPlayer.state != kStateDisconnected) {
                [[MultiPlayerNetwork sharedMultiPlayerNetwork] disconnect:YES];
            }
        }
    }
}

-(void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID{
    [[MultiPlayerNetwork sharedMultiPlayerNetwork] receiveInfos:[NSKeyedUnarchiver unarchiveObjectWithData:data]
                                                           from:peerID.displayName];
}


-(void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress{
    NSLog(@"Not implemented");
}


-(void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error{
    NSLog(@"Not implemented");
}


-(void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID{
    NSLog(@"Not implemented");
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    NSLog(@"Not implemented");
}

@end
