//
//  HomeController.m
//  MultiPeerConnectivity
//
//  Created by Fred on 20/10/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import "HomeController.h"
#import "GameController.h"
#import "kMPCConstants.h"

@interface HomeController ()
- (IBAction)SearchPlayer:(id)sender;

@property (nonatomic, strong) GameController *gameView;
@end

@implementation HomeController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:sMain
                                                         bundle:nil];
    self.gameView = [storyboard instantiateViewControllerWithIdentifier:vGame];
    
    [MultiPlayerNetwork sharedMultiPlayerNetwork].delegate = self.gameView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)SearchPlayer:(id)sender {
    //Affiche l'écran de selection des participants
    MCManager *manager = [MCManager sharedMCManager];
    [manager findMatchWithMinPlayers:2
                          maxPlayers:2
                      viewController:self];
    
    //Definit le manager à utiliser
    [[MultiPlayerNetwork sharedMultiPlayerNetwork] setManager:manager];
}

#pragma mark MC Delegate
-(void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController{
    //Libere la fenetre de selection des participant
    [browserViewController dismissViewControllerAnimated:YES
                                              completion:nil];
    
    [self.navigationController pushViewController:self.gameView
                                         animated:YES];
}

-(void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController{
    [browserViewController dismissViewControllerAnimated:YES
                                              completion:nil];
    [[MultiPlayerNetwork sharedMultiPlayerNetwork] disconnect:NO];
}
@end
